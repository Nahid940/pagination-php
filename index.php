<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/24/2017
 * Time: 10:43 PM
 */?>

<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/23/2017
 * Time: 11:39 PM
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/23/2017
 * Time: 10:37 PM
 */
    try {
        $pdo=new PDO('mysql:host=localhost;dbname=car_sales',"root","");
    }catch (PDOException $exp) {
        return $exp->getMessage();
    }

    if(isset($_GET['page'])){
        $pageNumber=$_GET['page'];
    }else{
        $pageNumber=0;
    }


    if($pageNumber==0 || $pageNumber==1){
        $page1=0;
    }else{
        $page1=($pageNumber*2)-2;
    }

    $sql="select * from car limit $page1,2";
    $stmt=$pdo->prepare($sql);
    $stmt->execute();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <table class="table">
                <tr>
                    <th>SL.</th>
                    <th>Name</th>
                </tr>
                <?php

                $i=0;
                foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $data){
                    $i++;

                ?>
                <tr>
                    <td><?php echo $i?></td>
                    <td><?php echo $data['model_code']?></td>
                </tr>
                <?php }?>
            </table>

            <?php
            $sql1="select * from car";
            $stmt1=$pdo->prepare($sql1);
            $stmt1->execute();
           $totalRow=$stmt1->rowCount();
             $perpage= $totalRow/2;
              
            ?>
                <ul class="pagination">
                <?php for($x=1;$x<=$perpage;$x++){?>
                <li><a href="index.php?page=<?php echo $x?>"><?php echo $x?></a></li>
                <?php }?>
                </ul>
            
        </div>
    </div>

</div>

</body>
</html>


